const express = require("express");
const cors = require("cors");

const app = express();
const port = 3000;

app.use(cors());
app.use(express.json());

const data = [
  { email: "jim@gmail.com", number: "221122" },
  { email: "jam@gmail.com", number: "830347" },
  { email: "john@gmail.com", number: "221122" },
  { email: "jams@gmail.com", number: "349425" },
  { email: "jams@gmail.com", number: "141424" },
  { email: "jill@gmail.com", number: "822287" },
  { email: "jill@gmail.com", number: "822286" },
];

let latestRequestId = 0;

app.get("/search", (req, res) => {
  const { email, number } = req.query;
  const requestId = Date.now(); 
  latestRequestId = requestId;
  if (!email || !number) {
    return res
      .status(400)
      .json({ error: "Both email and number parameters are required" });
  }

  setTimeout(() => {
    if (requestId !== latestRequestId) {
      return; 
    }
    const result = data.filter(
      (item) => item.email === email && item.number === number
    );
    if (result.length > 0) {
      res.json(result);
    } else {
      return res.status(400).json({ error: "User not found" });
    }
  }, 5000); 
});

app.listen(port, () => {
  console.log(`http://localhost:${port}`);
});
